﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Blog.Core.Domain.Identity
{
    [Table("AppUsers")]
    public class AppUser : IdentityUser<Guid>
    {
        [Required]
        [MaxLength(100)]
        public required string FirstName { get; set; }

        [Required]
        [MaxLength(100)]
        public required string LastName { get; set; }

        public bool IsActive { get; set; }

        public string? RefeshToken { get; set; }

        public DateTime RefeshTokenExpiryTime { get; set; }

        public DateTime DateCreated { get; set; }

        public DateTime Dob { get; set; }
        [MaxLength(500)]
        public string? Avatar { get; set; }
        public DateTime? VipStarDate { get; set; }
        public DateTime? VipExporeDate { get; set; }
        public DateTime? LastLogingDate { get; set; }
        public double Balance { get; set; }
    }
}
