﻿using Blog.Core.SeedWorks;
using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;


namespace Blog.Data.SeedWorks
{
    public class RepositoryBase<T, Key> : IRepository<T, Key> where T : class
    {
        protected readonly BlogDbContext _context;
        private readonly DbSet<T> _dbSet;
        public RepositoryBase(BlogDbContext blogDbContext )
        {
            _context = blogDbContext;
            _dbSet = _context.Set<T>();
        }
        public void Add(T entity)
        {
            _dbSet.Add( entity );   
        }

        public void AddRange(IEnumerable<T> entities)
        {
            _dbSet.AddRange( entities );
        }

        public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
        {
            return _dbSet.Where(expression);
        }

        public async Task<IEnumerable<T>> GeAllAsync()
        {
            return await _dbSet.ToListAsync();
        }

        public async Task<T> GetByIdASync(Key id) => await _dbSet.FindAsync(id) ?? throw new ArgumentException($"Something wrong when GetByIdASync: {id}");

        public void Remove(T entity)
        {
            _dbSet.Remove(entity);
        }

        public void RemoveRange(IEnumerable<T> entities)
        {
            _dbSet.RemoveRange(entities);
        }
    }
}
