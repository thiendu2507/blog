﻿using Blog.Core.Domain.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blog.Data
{
    public class DataSeeder
    {
        public async Task SeedAsync(BlogDbContext context)
        {
            var passwordHasher = new PasswordHasher<AppUser>();
            var rootAdminUserRoleId = Guid.NewGuid();
            if (!context.Roles.Any())
            {
                await context.Roles.AddAsync( new AppRole()
                {
                    Id = rootAdminUserRoleId,
                    Name = "Admin",
                    NormalizedName = "ADMIN",
                    DisplayName = "QTV",
                });
                await context.SaveChangesAsync();
            }
            if (!context.Users.Any())
            {
                var UserId = Guid.NewGuid();
                var user = new AppUser()
                {
                    Id = UserId,
                    FirstName = "Du",
                    LastName = "THien",
                    Email = "thiendu2507@gmail.com",
                    NormalizedEmail = "thiendu@gmail.com",
                    UserName = "ADMIN",
                    IsActive = true,
                    NormalizedUserName = "ADMIN",
                    SecurityStamp =  Guid.NewGuid().ToString(),
                    LockoutEnabled = false,
                    DateCreated = DateTime.Now,
                    
                };
                user.PasswordHash = passwordHasher.HashPassword(user, "admin@123");
                await context.Users.AddAsync(user);
                await context.UserRoles.AddAsync(new IdentityUserRole<Guid>()
                {
                    RoleId = rootAdminUserRoleId,
                    UserId = UserId,
                });
                await context.SaveChangesAsync();
            }
        }
    }
}
