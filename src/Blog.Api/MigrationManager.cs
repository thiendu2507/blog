﻿using Blog.Data;
using Microsoft.EntityFrameworkCore;
using System.Runtime.CompilerServices;

namespace Blog.Api
{
    public static class MigrationManager
    {
        public static WebApplication MigrateDatabase(this WebApplication app)
        {
            using (var scope = app.Services.CreateScope())
            {
                using(var context = scope.ServiceProvider.GetRequiredService<BlogDbContext>()) {
                    context.Database.Migrate();
                    new DataSeeder().SeedAsync(context).Wait();
                }
            }
            return app;
        }
    }
}
